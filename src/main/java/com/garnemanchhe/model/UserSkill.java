package com.garnemanchhe.model;

import javax.persistence.*;

@Entity
@Table(name = "user_skill")
public class UserSkill {
    @Id
    @Column(name = "user_skill_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer userSkillId;
    @Column(name = "skill_id")
    private int skillId;
    @Column(name = "user_detail_id")
    private int userDetailId;
    public UserSkill(){

    }

    public Integer getUserSkillId() {
        return userSkillId;
    }

    public void setUserSkillId(Integer userSkillId) {
        this.userSkillId = userSkillId;
    }

    public int getSkillId() {
        return skillId;
    }

    public void setSkillId(int skillId) {
        this.skillId = skillId;
    }

    public int getUserDetailId() {
        return userDetailId;
    }

    public void setUserDetailId(int userDetailId) {
        this.userDetailId = userDetailId;
    }
}

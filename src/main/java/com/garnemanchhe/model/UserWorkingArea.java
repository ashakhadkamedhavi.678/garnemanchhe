package com.garnemanchhe.model;

import javax.persistence.*;

@Entity
@Table(name = "user_working_area")
public class UserWorkingArea {
    @Id
    @Column(name = "user_working_area_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer userWorkingAreaId;
    @Column(name = "working_area_id")
    private int workingAreaId;
    @Column(name = "user_details_id")
    private int userDetailId;

    public UserWorkingArea(){

    }

    public Integer getUserWorkingAreaId() {
        return userWorkingAreaId;
    }

    public void setUserWorkingAreaId(Integer userWorkingAreaId) {
        this.userWorkingAreaId = userWorkingAreaId;
    }

    public int getWorkingAreaId() {
        return workingAreaId;
    }

    public void setWorkingAreaId(int workingAreaId) {
        this.workingAreaId = workingAreaId;
    }

    public int getUserDetailId() {
        return userDetailId;
    }

    public void setUserDetailId(int userDetailId) {
        this.userDetailId = userDetailId;
    }
}

package com.garnemanchhe.model;

import javax.persistence.*;

@Entity
@Table(name = "working_table")
public class WorkingArea {
    @Id
    @Column(name = "working_area_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer workingAreaId;
    @Column(name = "district_name")
    private String districtName;
    @Column(name = "localbody")
    private String localbody;
    @Column(name = "street_address")
    private String streetAddress;
    @Column(name = "ward_number")
    private String wardNumber;

    public WorkingArea(){

    }

    public Integer getWorkingAreaId() {
        return workingAreaId;
    }

    public void setWorkingAreaId(Integer workingAreaId) {
        this.workingAreaId = workingAreaId;
    }

    public String getDistrictName() {
        return districtName;
    }

    public void setDistrictName(String districtName) {
        this.districtName = districtName;
    }

    public String getLocalbody() {
        return localbody;
    }

    public void setLocalbody(String localbody) {
        this.localbody = localbody;
    }

    public String getStreetAddress() {
        return streetAddress;
    }

    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    public String getWardNumber() {
        return wardNumber;
    }

    public void setWardNumber(String wardNumber) {
        this.wardNumber = wardNumber;
    }
}

package com.garnemanchhe.service;

import com.garnemanchhe.model.UserDetail;

import java.util.List;

public interface UserDetailService {
    void save(UserDetail userDetail);
    List<UserDetail> findAll();
    void deleteById(Integer userDetailId);
}

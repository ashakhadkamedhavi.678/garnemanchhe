package com.garnemanchhe.service;

import com.garnemanchhe.model.Skill;
import com.garnemanchhe.repository.SkillRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SkillServiceImpl implements SkillService{
    @Autowired
    private SkillRepository skillRepository;


    @Override
    public void save(Skill skill) {
        skillRepository.save(skill);
    }

    @Override
    public List<Skill> findAll() {
        return skillRepository.findAll();
    }

    @Override
    public void deleteById(Integer skillId) {
        skillRepository.deleteById(skillId);

    }
}

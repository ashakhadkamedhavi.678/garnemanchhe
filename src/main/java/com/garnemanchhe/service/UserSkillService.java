package com.garnemanchhe.service;

import com.garnemanchhe.model.UserDetail;
import com.garnemanchhe.model.UserSkill;

import java.util.List;

public interface UserSkillService {
    void save(UserSkill userSkill);
    List<UserSkill> findAll();
    void deleteById(Integer userSkillId);
}

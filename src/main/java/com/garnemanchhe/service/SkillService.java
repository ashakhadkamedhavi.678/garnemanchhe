package com.garnemanchhe.service;

import com.garnemanchhe.model.Skill;

import java.util.List;

public interface SkillService {
    void save(Skill skill);
    List<Skill> findAll();
    void deleteById(Integer skillId);
}

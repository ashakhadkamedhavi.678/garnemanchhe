package com.garnemanchhe.service;

import com.garnemanchhe.model.UserSkill;
import com.garnemanchhe.repository.UserSkillRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserSkillServiceImpl implements UserSkillService{
    @Autowired
    private UserSkillRepository userSkillRepository;

    @Override
    public void save(UserSkill userSkill) {
        userSkillRepository.save(userSkill);
    }

    @Override
    public List<UserSkill> findAll() {
        return userSkillRepository.findAll();
    }

    @Override
    public void deleteById(Integer userSkillId) {
        userSkillRepository.deleteById(userSkillId);
    }
}

package com.garnemanchhe.service;

import com.garnemanchhe.model.UserWorkingArea;
import com.garnemanchhe.repository.UserWorkingAreaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserWorkingAreaServiceImpl implements UserWorkingAreaService{
    @Autowired
    private UserWorkingAreaRepository userWorkingAreaRepository;

    @Override
    public void save(UserWorkingArea userWorkingArea) {
        userWorkingAreaRepository.save(userWorkingArea);
    }

    @Override
    public List<UserWorkingArea> findAll() {
        return userWorkingAreaRepository.findAll();
    }

    @Override
    public void deleteById(Integer userWorkingAreaId) {
        userWorkingAreaRepository.deleteById(userWorkingAreaId);

    }
}

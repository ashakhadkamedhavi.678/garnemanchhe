package com.garnemanchhe.service;

import com.garnemanchhe.model.UserDetail;
import com.garnemanchhe.model.UserWorkingArea;

import java.util.List;

public interface UserWorkingAreaService {
    void save(UserWorkingArea userWorkingArea);
    List<UserWorkingArea> findAll();
    void deleteById(Integer userWorkingAreaId);
}

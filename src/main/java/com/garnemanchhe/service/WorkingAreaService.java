package com.garnemanchhe.service;

import com.garnemanchhe.model.UserDetail;
import com.garnemanchhe.model.WorkingArea;

import java.util.List;

public interface WorkingAreaService {
    void save(WorkingArea workingArea);
    List<WorkingArea> findAll();
    void deleteById(Integer workingAreaId);
}

package com.garnemanchhe.service;

import com.garnemanchhe.model.UserDetail;
import com.garnemanchhe.repository.UserDetailRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserDetailServiceImpl implements UserDetailService{
    @Autowired
    private UserDetailRepository userDetailRepository;

    @Override
    public void save(UserDetail userDetail) {
        userDetailRepository.save(userDetail);

    }

    @Override
    public List<UserDetail> findAll() {
        return userDetailRepository.findAll();
    }

    @Override
    public void deleteById(Integer userDetailId) {
        userDetailRepository.deleteById(userDetailId);
    }
}

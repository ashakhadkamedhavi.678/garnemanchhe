package com.garnemanchhe.service;

import com.garnemanchhe.model.WorkingArea;
import com.garnemanchhe.repository.WorkingAreaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WorkingAreaServiceImpl implements WorkingAreaService{
    @Autowired
    private WorkingAreaRepository workingAreaRepository;


    @Override
    public void save(WorkingArea workingArea) {
        workingAreaRepository.save(workingArea);
    }

    @Override
    public List<WorkingArea> findAll() {
        return workingAreaRepository.findAll();
    }

    @Override
    public void deleteById(Integer workingAreaId) {
        workingAreaRepository.deleteById(workingAreaId);

    }
}

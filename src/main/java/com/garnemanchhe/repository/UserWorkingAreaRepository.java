package com.garnemanchhe.repository;

import com.garnemanchhe.model.UserWorkingArea;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserWorkingAreaRepository extends JpaRepository <UserWorkingArea, Integer> {
}

package com.garnemanchhe.repository;

import com.garnemanchhe.model.Skill;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SkillRepository extends JpaRepository <Skill, Integer> {
}

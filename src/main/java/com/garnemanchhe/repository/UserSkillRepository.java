package com.garnemanchhe.repository;

import com.garnemanchhe.model.UserSkill;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserSkillRepository extends JpaRepository <UserSkill, Integer> {
}

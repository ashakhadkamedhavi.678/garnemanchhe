package com.garnemanchhe.repository;

import com.garnemanchhe.model.WorkingArea;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WorkingAreaRepository extends JpaRepository <WorkingArea, Integer> {
}

package com.garnemanchhe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GarnemanchheApplication {

    public static void main(String[] args) {
        SpringApplication.run(GarnemanchheApplication.class, args);
    }

}

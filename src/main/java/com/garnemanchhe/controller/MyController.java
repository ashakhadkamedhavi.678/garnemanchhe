package com.garnemanchhe.controller;

import com.garnemanchhe.model.*;
import com.garnemanchhe.repository.WorkingAreaRepository;
import com.garnemanchhe.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class MyController {
    @GetMapping("/homepage")
    public String getHomepage(Model model){

        return "homepage";
    }
    @GetMapping("/register")
    public String getIndex(Model model){

        return "register";
    }
    @GetMapping("/login")
    public String getLogin(Model model){

        return "login";
    }
    @GetMapping("/update")
    public String getUpdate(Model model){

        return "update";
    }
    @GetMapping("/reset")
    public String getReset(Model model){

        return "reset";
    }
    @Autowired
    private SkillService skillService;
    @PostMapping("save/skill")
    public  String saveSkill(@RequestParam ("skill") String skill){
        Skill s = new Skill();
        s.setSkill(skill);
        skillService.save(s);
        return "redirect:/register";
    }
    private UserDetailService userDetailService;
    @PostMapping("save/user_detail")
    public  String saveUserDetail(@RequestParam ("fullname") String fullname,
                                  @RequestParam ("address") String address,
                                  @RequestParam ("mobile_number") String mobile_number){
        UserDetail userDetail = new UserDetail();
        userDetail.setFullname(fullname);
        userDetail.setAddress(address);
        userDetail.setMobileNumber(mobile_number);
        userDetailService.save(userDetail);
        return "redirect:/register";
    }
    private UserSkillService userSkillService;
    @PostMapping("save/user_skill")
    public  String saveUserSkill(@RequestParam ("skill_id") int skill_id,
                               @RequestParam ("user_detail_id") int user_detail_id){
        UserSkill userSkill = new UserSkill();
        userSkill.setSkillId(skill_id);
        userSkill.setUserDetailId(user_detail_id);
        userSkillService.save(userSkill);
        return "redirect:/register";
    }


    private WorkingAreaService workingAreaService;
    @PostMapping("save/working_area")
    public  String saveWorkingArea(@RequestParam ("district_name") String district_name,
                                 @RequestParam ("localbody") String localbody,
                                 @RequestParam ("street_address") String street_address,
                                 @RequestParam ("ward_number") String ward_number
    ){
        WorkingArea workingArea = new WorkingArea();
        workingArea.setDistrictName(district_name);
        workingArea.setLocalbody(localbody);
        workingArea.setStreetAddress(street_address);
        workingArea.setWardNumber(ward_number);
        workingAreaService.save(workingArea);
        return "redirect:/register";
    }
    private UserWorkingAreaService userWorkingAreaService;
    @PostMapping("save/user_working_area")
    public  String saveUserWorkingArea(@RequestParam ("working_area_id") int working_area_id,
                                     @RequestParam ("user_detail_id") int user_detail_id){
        UserWorkingArea userWorkingArea = new UserWorkingArea();
        userWorkingArea.setWorkingAreaId(working_area_id);
        userWorkingAreaService.save(userWorkingArea);
        return "redirect:/register";

    }
}

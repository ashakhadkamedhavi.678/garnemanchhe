<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
  <title>Garne Manchhe login</title>
    <jsp:include page="fragments/header.jsp"></jsp:include>
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <form method="post" class="form-login" data-toggle="validator" action="/login">
                <img src="images/login.png" class="login-img">
                <div class="form-group">
                    <input type="text" class="form-control login" placeholder="Username *" value=""  required />
                </div>
                <div class="form-group">
                    <input type="password" class="form-control login" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters" placeholder="Your Password *" placeholder="Your Password *" value="" required />
                </div>
                <div class="form-group">
                    <input type="submit" class="btn-login" value="login" />
                </div>
                <div class="form-group text-center">
                    <a href="/reset" class="ForgetPwd">Forget Password?</a>
                </div>
            </form>
        </div>
        <div class="col-md-4">
        </div>
    </div>
</div>
  <jsp:include page="fragments/footer.jsp"></jsp:include>
</body>
</html>
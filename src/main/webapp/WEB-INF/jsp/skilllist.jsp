<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
  <title>Skill List</title>
	<jsp:include page="fragments/header.jsp"></jsp:include>
</head>
<body>
	<div class="container-fluid">
		<table class="table table-success table-striped">
			<thead>
			    <tr>
			      <th scope="col">Skill Id</th>
			      <th scope="col">Skill</th>
			    </tr>
			</thead>
			  <tbody>
			    <tr>
					<c:forEach items="${skilllist}" var="skill">
						<th scope="row">${skill.skillId}</th>
						<td>${skill.skill}</td>
					</c:forEach>
			    </tr>
			  </tbody>
        </table>
	</div>
	<jsp:include page="fragments/footer.jsp"></jsp:include>
 </body>
</html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
  <title>User Working Area List</title>
	<jsp:include page="fragments/header.jsp"></jsp:include>
</head>
<body>
	<div class="container-fluid">
		<table class="table table-success table-striped">
			<thead>
			    <tr>
			      <th scope="col">User Working Area Id</th>
			      <th scope="col">Working Area Id</th>
			      <th scope="col">User Detail Id</th>
			    </tr>
			</thead>
			  <tbody>
			    <tr>
					<c:forEach items="userworkingarealist" var="userworkingarea">
					  <th scope="row">${userworkingarea.userWorkingAreaId}</th>
					  <td>${userworkingarea.workingAreaId}</td>
					  <td>${userworkingarea.userDetailId}</td>
					</c:forEach>
			    </tr>
			    <tr>
			      <th scope="row">2</th>
			      <td>2</td>
			      <td>2</td>
			    </tr>
			  </tbody>
        </table>
	</div>
	<jsp:include page="fragments/footer.jsp"></jsp:include>
 </body>
</html>
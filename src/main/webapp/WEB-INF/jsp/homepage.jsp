<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
  <title>Garne Manchhe home page</title>
  <jsp:include page="fragments/header.jsp"></jsp:include>
</head>
<body>
<div class="container-fluid">
  <div class="col-md-3"></div>
  <div class="col-md-6">
    <form method="post" class="form-home" data-toggle="validator">
      <img src="images/GarneManchheLogo.png" class="center">
      <div class="form-group">
        <input type="text" name="WorkingArea" class="form-control home"  placeholder="WorkingArea" required><br/>
      </div>
      <div class="form-group">
        <input type="text" name="skill" class="form-control home" placeholder="Skill Of A Person You Are Searching" required><br/>
      </div>
      <div class="row">
        <div class="col-md-4 col-sm-4 col-xs-4">
          <button type="search" class="btn-search home-search">search</button>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-4 ">
          <button type="update" class="btn-update home-update"><a href="/update">update</a></button>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-4">
          <button type="register" class="btn-register home-register"><a href="/register">register</a></button>
        </div>
      </div>
    </form>
  </div>
  <div class="col-md-3"></div>
</div>
    <jsp:include page="fragments/footer.jsp"></jsp:include>
  </body>
</html>
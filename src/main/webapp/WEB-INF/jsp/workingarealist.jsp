<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
  <title>Working Area List</title>
	<jsp:include page="fragments/header.jsp"></jsp:include>
</head>
<body>
	<div class="container-fluid">
		<table class="table table-success table-striped">
			<thead>
			    <tr>
			      <th scope="col">Working Area Id</th>
			      <th scope="col">District Name</th>
			      <th scope="col">Localbody</th>
			      <th scope="col">Street Address</th>
			      <th scope="col">Ward Number</th>
			    </tr>
			</thead>
			  <tbody>
			    <tr>
					<c:forEach items="workingarealist" var="workingarea">
					  <th scope="row">${workingarea.workingAreaId}</th>
					  <td>${workingarea.districtName}</td>
					  <td>${workingarea.localbody}</td>
					  <td>${workingarea.streetAddress}</td>
					  <td>${workingarea.wardNumber}</td>
					</c:forEach>
			    </tr>
			  </tbody>
        </table>
	</div>
	<jsp:include page="fragments/footer.jsp"></jsp:include>
 </body>
</html>
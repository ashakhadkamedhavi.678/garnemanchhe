<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
  <title>Garne Manchhe Update Registration Form</title>
  <jsp:include page="fragments/header.jsp"></jsp:include>
</head>
<body>
<div class="container-fluid">
  <div class="row">
    <div class="col-md-2"></div>
    <div class="col-md-8">
      <form method="post" class="form" data-toggle="validator">
        <div class="row">
          <div class="col-md-5">
            <img class="img" src="images/GarneManchheLogo.png">
          </div>
          <div class="col-md-7">
            <h2 class="h2">ENTER YOUR DETAILS</h2>
            <div class="form-group">
              <label class="fullname">FullName</label>
              <input type="text" name="fullname" class="form-control r" pattern="^([A-Z]{1}[a-z]{1,}\s)([A-Z]{1}[a-z]{1,}\s?)+$"  placeholder="" required>
            </div>
            <div class="form-group">
              <label class="address">Address</label>
              <input type="text" name="address" class="form-control r"  placeholder="">
            </div>
            <div class="form-group">
              <label class="skill">SKill</label>
              <input type="text" name="skill" class="form-control r" placeholder="" required>
            </div>
            <div class="form-group">
              <label class="mobilenumber">MobileNumber</label>
              <input type="text" name="mobilenumber" class="form-control r" pattern="^\d{10}$" placeholder="" required>
            </div>
            <div class="form-group">
              <label class="workingarea">WorkingArea</label>
              <div class="row">
                <div class="col-md-6">
                  <label class="districtname">DistrictName</label>
                  <input type="text" name="districtname" class="form-control r"  placeholder=""><br/>
                </div>
                <div class="col-md-6">
                  <label class="localbody">LocalBody</label>
                  <input type="text" name="localbody" class="form-control r"  placeholder=""><br/>
                </div>
                <div class="col-md-6">
                  <label class="streetaddress">StreetAddress</label>
                  <input type="text" name="streetaddress" class="form-control r"  placeholder="" required><br/>
                </div>
                <div class="col-md-6">
                  <label class="wardno">WardNumber</label>
                  <input type="text" name="wardno" class="form-control r" placeholder=""><br/>
                </div>
              </div>
            </div>
            <div class="form-group">
              <button type="submit" class="btn-update">update</button>
            </div>
          </div>
        </div>
      </form>
    </div>
    <div class="col-md-2"></div>
  </div>
</div>
    <jsp:include page="fragments/footer.jsp"></jsp:include>
 </body>
</html>
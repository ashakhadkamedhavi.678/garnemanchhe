<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
  <title>User Detail List</title>
	<jsp:include page="fragments/header.jsp"></jsp:include>
</head>
<body>
	<div class="container-fluid">
		<table class="table table-success table-striped">
			<thead>
			    <tr>
			      <th scope="col">User Detail Id</th>
			      <th scope="col">FullName</th>
			      <th scope="col">Address</th>
			      <th scope="col">Mobile Number</th>
			    </tr>
			</thead>
			  <tbody>
			    <tr>
					<c:forEach items="${userdetaillist}" var="userdetail">
					  <th scope="row">${userdetail.userDetailId}</th>
					  <td>${userdetail.fullname}</td>
					  <td>${userdetail.address}</td>
					  <td>${userdetail.mobileNumber}</td>
					</c:forEach>
			    </tr>
			  </tbody>
        </table>
	</div>
	<jsp:include page="fragments/footer.jsp"></jsp:include>
 </body>
</html>
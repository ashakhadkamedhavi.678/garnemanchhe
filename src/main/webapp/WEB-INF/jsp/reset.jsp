<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
  <title>Garne Manchhe reset</title>
    <jsp:include page="fragments/header.jsp"></jsp:include>
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <form method="post" class="form-reset" data-toggle="validator">
                <img src="images/reset.png" class="reset-img">
                <div class="form-group">
                    <input type="text" class="form-control reset" placeholder="Username *" value=""  required />
                </div>
                <div class="form-group">
                    <input type="text" class="form-control reset" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters" placeholder="Your Password *" value="" required />
                </div>
                <div class="form-group">
                    <input type="submit" class="btn-reset" value="reset" />
                </div>
            </form>
        </div>
        <div class="col-md-4">
        </div>
    </div>
</div>
  <jsp:include page="fragments/footer.jsp"></jsp:include>
</body>
</html>
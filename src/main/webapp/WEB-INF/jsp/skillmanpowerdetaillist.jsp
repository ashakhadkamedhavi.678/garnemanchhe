<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
  <title>Skil Manpower Detail List</title>
	<jsp:include page="fragments/header.jsp"></jsp:include>
</head>
<body>
	<div class="container-fluid">
		<table class="table table-success table-striped">
			<thead>
			<tr>
				<th rowspan="2">S.N</th>
			    <th rowspan="2">Fullname<br></th>
			    <th rowspan="2">Address</th>
			    <th rowspan="2">Skill</th>
			    <th rowspan="2">Mobile Number</th>
			    <th colspan="4" style="text-align:center">Working Area<br></th>
			</tr>
			<tr>
			    <th>District Name</th>
			    <th>Localbody</th>
			    <th>Street Address</th>
			    <th>Ward Number</th>
			</tr>	
				<tbody>
					<tr>
						<c:forEach item="skillmanpowerdetaillist" var="skillmanpowerdetail">
						<td>${skillmanpowerdetail.sn}</td>
					    <td>${skillmanpowerdetail.fullname}</td>
					    <td>${skillmanpowerdetail.address}</td>
					    <td>${skillmanpowerdetail.skill}</td>
					    <td>${skillmanpowerdetail.mobileNumber}</td>
					    <td>${skillmanpowerdetail.districtName}</td>
					    <td>${skillmanpowerdetail.localbody}</td>
					    <td>${skillmanpowerdetail.streeAdress}</td>
					    <td>${skillmanpowerdetail.wardNumber}</td>
						</c:forEach>
					</tr>
				</tbody>
			</thead>
		</table>
	</div>
	<jsp:include page="fragments/footer.jsp"></jsp:include>
 </body>
</html>
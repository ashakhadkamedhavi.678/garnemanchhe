<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
  <title>User Skill List</title>
	<jsp:include page="fragments/header.jsp"></jsp:include>
</head>
<body>
	<div class="container-fluid">
		<table class="table table-success table-striped">
			<thead>
			    <tr>
			      <th scope="col">User Skill Id</th>
			      <th scope="col">Skill Id</th>
			      <th scope="col">User Detail Id</th>
			    </tr>
			</thead>
			  <tbody>
			    <tr>
					<c:forEach items="${userskilllist}" var="userskill">
					  <th scope="row">${userskill.userSkillId}</th>
					  <td>${userskill.userDetailId}</td>
					  <td>${userskill.skillID}</td>
					</c:forEach>
			    </tr>
			  </tbody>
        </table>
	</div>
	<jsp:include page="fragments/footer.jsp"></jsp:include>
 </body>
</html>